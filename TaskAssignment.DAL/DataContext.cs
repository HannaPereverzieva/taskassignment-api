﻿using Microsoft.EntityFrameworkCore;
using TaskAssignment.DAL.Entities;

namespace TaskAssignment.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
           : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TaskGroup> TaskGroups { get; set; }
        public DbSet<UserTask> UserTasks { get; set; }
    }
}
