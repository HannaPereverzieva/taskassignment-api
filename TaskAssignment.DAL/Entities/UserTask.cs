﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TaskAssignment.DAL.Entities.Enums;

namespace TaskAssignment.DAL.Entities
{
    public class UserTask : EntityBase
    {
        public string Name { get; set; }
        public DateTime DeadlineDate { get; set; }
        public eTaskStatus Status { get; set; } = eTaskStatus.New;

        [ForeignKey("TaskGroup")]
        public int TaskGroupId { get; set; }
        public TaskGroup TaskGroup { get; set; }

        [ForeignKey("User")]
        public int? UserId { get; set; }
        public User User { get; set; }
    }
}
