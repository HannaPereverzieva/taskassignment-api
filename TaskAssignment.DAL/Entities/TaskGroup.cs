﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskAssignment.DAL.Entities
{
    public class TaskGroup : EntityBase
    {
        public string Name { get; set; }
        public ICollection<UserTask> UserTasks { get; set; }
    }
}
