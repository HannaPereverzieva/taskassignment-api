﻿namespace TaskAssignment.DAL.Entities.Enums
{
    public enum eTaskStatus
    {
        New = 1,
        InProgress = 2,
        Completed = 3,
    }
}
