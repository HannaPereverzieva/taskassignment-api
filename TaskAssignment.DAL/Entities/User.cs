﻿using System.Collections.Generic;

namespace TaskAssignment.DAL.Entities
{
    public class User: EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<UserTask> UserTasks { get; set; }
    }
}
