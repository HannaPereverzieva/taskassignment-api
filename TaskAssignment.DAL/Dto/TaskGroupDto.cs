﻿namespace TaskAssignment.DAL.Dto
{
    public class TaskGroupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TasksCount { get; set; }
    }
}
