﻿using Microsoft.Extensions.Configuration;

namespace TaskAssignment.DAL
{
    public static class ModuleConfig
    {
        public static IConfigurationRoot Configuration
        {
            get
            {
                var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                return builder.Build();
            }
        }
    }
}
