﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;


namespace TaskAssignment.DAL
{
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            return new DataContext(new DbContextOptionsBuilder<DataContext>()
                .UseSqlServer(ModuleConfig.Configuration.GetConnectionString("MigrationConnection")).Options);
        }
    }
}
