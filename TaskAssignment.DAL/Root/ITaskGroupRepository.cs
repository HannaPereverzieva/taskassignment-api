﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaskAssignment.DAL.Dto;
using TaskAssignment.DAL.Entities;

namespace TaskAssignment.DAL.Root
{
    public interface ITaskGroupRepository : IGenericRepository<TaskGroup>
    {
        Task<IEnumerable<TaskGroupDto>> GetAllWithTasksCount();
    }
}
