﻿using System;
using System.Threading.Tasks;

namespace TaskAssignment.DAL.Root
{
    public interface IGenericUnitOfWork : IDisposable
    {
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
        Task SaveAsync();
    }
}