﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAssignment.DAL.Entities;

namespace TaskAssignment.DAL.Root.Repository
{
    public class GenericUnitOfWork : IGenericUnitOfWork
    {
        private DataContext _dbContext;
        private bool disposed = false;
        public Dictionary<Type, object> Repositories = new Dictionary<Type, object>();
        public GenericUnitOfWork(DataContext context)
        {
            _dbContext = context;
        }

        public IGenericRepository<T> GetRepository<T>() where T : class
        {
            if (Repositories.Keys.Contains(typeof(T)) == true)
                return Repositories[typeof(T)] as IGenericRepository<T>;

            if (typeof(T) == typeof(TaskGroup))
                Repositories.Add(typeof(T), new TaskGroupRepository(_dbContext));
            else
                Repositories.Add(typeof(T), new GenericRepository<T>(_dbContext));

            return Repositories[typeof(T)] as IGenericRepository<T>;       
        }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            if (!disposed)
            {
                _dbContext.Dispose();
            }
            disposed = true;
        }
    }
}
