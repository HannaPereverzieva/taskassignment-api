﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TaskAssignment.DAL.Root.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected DataContext db;

        protected DbSet<T> dbSet;
        public GenericRepository(DataContext _db)
        {
            db = _db;
            dbSet = db.Set<T>();
        }
        public async virtual Task Add(T entity)
        {
            await dbSet.AddAsync(entity);
        }

        public virtual void Update(T entity)
        {
            dbSet.Update(entity);
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public async virtual Task<T> GetDetails(Expression<Func<T, bool>> predicate, string[] includes = null)
        {
            var query = QueryObject();

            if (includes != null)
            {
                foreach (var item in includes)
                {
                    query = query.Include(item);
                }
            }

            if (predicate != null)
                query = query.Where(predicate);

            return await query.FirstOrDefaultAsync(predicate);
        }

        public async virtual Task<IEnumerable<T>> GetOverview(Expression<Func<T, bool>> predicate = null, string[] includes = null)
        {
            var query = QueryObject();

            if (includes != null)
            {
                foreach (var item in includes)
                {
                    query = query.Include(item);
                }
            }

            if (predicate != null)
                query = query.Where(predicate);
      
            return await query.ToListAsync();
        }
        protected virtual IQueryable<T> QueryObject()
        {
            return dbSet.AsQueryable();
        }
    }
}
