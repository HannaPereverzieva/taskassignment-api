﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAssignment.DAL.Dto;
using TaskAssignment.DAL.Entities;

namespace TaskAssignment.DAL.Root.Repository
{
    public class TaskGroupRepository : GenericRepository<TaskGroup>, ITaskGroupRepository
    {
        public TaskGroupRepository(DataContext db) : base(db)
        {
        }

        public async Task<IEnumerable<TaskGroupDto>> GetAllWithTasksCount()
        {
            return await db.TaskGroups
                    .Include(d => d.UserTasks)
                    .Select(s => new TaskGroupDto() { Id = s.Id, Name = s.Name, TasksCount = s.UserTasks.Count() })
                    .ToListAsync();
        }
    }
}
