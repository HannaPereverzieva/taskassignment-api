﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TaskAssignment.DAL.Root
{
    public interface IGenericRepository<T> where T : class
    {
       // Task<IEnumerable<T>> GetOverview(Expression<Func<T, bool>>[] predicate, string[] includes = null);
        Task<IEnumerable<T>> GetOverview(Expression<Func<T, bool>> predicate = null, string[] includes = null);
        Task<T> GetDetails(Expression<Func<T, bool>> predicate, string[] includes = null);
        Task Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
