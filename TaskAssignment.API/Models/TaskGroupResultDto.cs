﻿namespace TaskAssignment.API.Models
{
    public class TaskGroupResultDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
