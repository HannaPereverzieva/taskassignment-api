﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskAssignment.API.Models
{
    public class GroupWithTasksCountResultDto: TaskGroupResultDto
    {
        public int TasksCount { get; set; }
    }
}
