﻿namespace TaskAssignment.API.Models
{
    public class UserResultDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
