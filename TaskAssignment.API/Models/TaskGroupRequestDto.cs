﻿namespace TaskAssignment.API.Models
{
    public class TaskGroupRequestDto
    {
        public string Name { get; set; }
    }
}
