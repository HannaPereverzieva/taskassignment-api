﻿using System.Collections.Generic;

namespace TaskAssignment.API.Models
{
    public class GroupDetailsResultDto: TaskGroupResultDto
    {
        public List<UserTaskResultDto> UserTasks { get; set; }
    }
}
