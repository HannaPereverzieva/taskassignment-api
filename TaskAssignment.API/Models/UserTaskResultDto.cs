﻿using System;
using TaskAssignment.DAL.Entities.Enums;

namespace TaskAssignment.API.Models
{
    public class UserTaskResultDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DeadlineDate { get; set; }
        public eTaskStatus Status { get; set; }
        public int UserId { get; set; }
    }
}
