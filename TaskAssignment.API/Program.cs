using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskAssignment.DAL;
using TaskAssignment.DAL.Entities;

namespace TaskAssignment.API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var serviceScope = host.Services.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<DataContext>();

                await dbContext.Database.MigrateAsync();
                if (!dbContext.Users.AsQueryable().Any())
                {
                    var list = new List<User>() { 
                        new User() { FirstName = "Krystian", LastName = "Nowak" },
                        new User() { FirstName = "Maciej", LastName = "Kowalski" }, 
                        new User() { FirstName = "Zbigniew", LastName = "Czajka" } };
                    await dbContext.AddRangeAsync(list);
                    await dbContext.SaveChangesAsync();
                }
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
