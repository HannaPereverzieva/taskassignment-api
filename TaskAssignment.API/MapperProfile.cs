﻿using AutoMapper;
using TaskAssignment.API.Models;
using TaskAssignment.DAL.Dto;
using TaskAssignment.DAL.Entities;

namespace TaskAssignment.API
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, UserResultDto>();
            CreateMap<TaskGroupDto, GroupWithTasksCountResultDto>();
            CreateMap<TaskGroupRequestDto, TaskGroup>();
            CreateMap<UserTaskRequestDto, UserTask>();
            CreateMap<TaskGroup, GroupDetailsResultDto>();
            CreateMap<TaskGroup, TaskGroupResultDto>();
            CreateMap<UserTask, UserTaskResultDto>();
            CreateMap<TaskGroup, GroupDetailsResultDto>();
        }
    }
}
