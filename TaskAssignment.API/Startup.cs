using AutoMapper;
using HR.API.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using TaskAssignment.DAL;
using TaskAssignment.DAL.Root;
using TaskAssignment.DAL.Root.Repository;

namespace TaskAssignment.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IWebHostEnvironment environment)
        {
            var configurationBuilder = new ConfigurationBuilder()
               .AddJsonFile($"appsettings.json", false)
               .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", false);

            Configuration = configurationBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddAutoMapper(typeof(Startup));
            services.AddTransient<IGenericUnitOfWork, GenericUnitOfWork>();

            // swagger
            services.ConfigureSwaggerGen(options => options.CustomSchemaIds(x => x.FullName));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Task assignment API", Version = "v1", Description = "Test API with ASP.NET Core 3.0" });
                c.EnableAnnotations();
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCorsMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", String.Format("Task Assigment API. Build: {0}. Environment: {1}.",
                    Environment.GetEnvironmentVariable("BUILD_NUMBER"),
                    env.EnvironmentName));
                c.RoutePrefix = string.Empty;
                c.DocExpansion(DocExpansion.List);
            });
        }
    }
}
