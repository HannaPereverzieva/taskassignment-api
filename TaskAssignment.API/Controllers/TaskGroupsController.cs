﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TaskAssignment.API.Models;
using TaskAssignment.DAL.Dto;
using TaskAssignment.DAL.Entities;
using TaskAssignment.DAL.Root;
namespace TaskAssignment.API.Controllers
{
    [Produces("application/json")]
    [Route("api/groups")]
    public class TaskGroupsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericUnitOfWork _uow;
        public TaskGroupsController(IMapper mapper, IGenericUnitOfWork uow)
        {
            _mapper = mapper;
            _uow = uow;
        }

        [HttpGet]
        [SwaggerOperation(Tags = new[] { "Groups" })]
        [ProducesResponseType(typeof(IEnumerable<GroupWithTasksCountResultDto>), 200)]
        public async Task<IActionResult> GetGroups()
        {
            var repository = _uow.GetRepository<TaskGroup>() as ITaskGroupRepository;
            var groups = await repository.GetAllWithTasksCount();
            if (groups == null)
                return NoContent();
            var result = _mapper.Map<IEnumerable<TaskGroupDto>, IEnumerable<GroupWithTasksCountResultDto>>(groups);
            return Ok(result);
        }

        [HttpGet]
        [Route("{groupId}")]
        [SwaggerOperation(Tags = new[] { "Groups" })]
        [ProducesResponseType(typeof(GroupDetailsResultDto), 200)]
        public async Task<IActionResult> GetGroup([FromRoute]int groupId)
        {
            var group = await _uow.GetRepository<TaskGroup>().GetDetails(x => x.Id == groupId, new string[] { "UserTasks" });
            if (group == null)
                return NoContent();
            var result = _mapper.Map<TaskGroup, GroupDetailsResultDto>(group);
            return Ok(result);
        }

        [HttpPost]
        [SwaggerOperation(Tags = new[] { "Groups" })]
        [ProducesResponseType(typeof(TaskGroupResultDto), 201)]
        public async Task<IActionResult> AddGroup([FromBody]TaskGroupRequestDto groupDto)
        {
            var entity = _mapper.Map<TaskGroupRequestDto, TaskGroup>(groupDto);
            await _uow.GetRepository<TaskGroup>().Add(entity);
            await _uow.SaveAsync();
            var result = _mapper.Map<TaskGroup, TaskGroupResultDto>(entity);
            return CreatedAtAction("GetGroup", new { groupId = result.Id }, result);
        }

        [HttpPatch]
        [Route("{groupId}")]
        [SwaggerOperation(Tags = new[] { "Groups" })]
        [ProducesResponseType(204)]
        public async Task<IActionResult> UpdateGroup([FromRoute]int groupId, [FromBody]TaskGroupRequestDto groupDto)
        {
            var repository = _uow.GetRepository<TaskGroup>();
            var existGroup = await repository.GetDetails(x => x.Id == groupId);
            if (existGroup != null)
            {
                _mapper.Map(groupDto, existGroup);
                await _uow.SaveAsync();
                return Ok();
            }
            return NotFound();
        }

        [HttpDelete]
        [Route("{groupId}")]
        [SwaggerOperation(Tags = new[] { "Groups" })]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteTask([FromRoute]int groupId)
        {
            var repository = _uow.GetRepository<TaskGroup>();
            var entity = await repository.GetDetails(x => x.Id == groupId);
            if (entity != null)
            {
                repository.Delete(entity);
                await _uow.SaveAsync();
                return Ok();
            }
            return NotFound();
        }

        [HttpGet]
        [Route("{groupId}/tasks")]
        [SwaggerOperation(Tags = new[] { "Groups" })]
        [ProducesResponseType(typeof(IEnumerable<UserTaskResultDto>), 200)]
        public async Task<IActionResult> GetTasksPerGroup([FromRoute]int groupId)
        {
            var tasks = await _uow.GetRepository<UserTask>().GetOverview(x => x.TaskGroupId == groupId);
            if (tasks == null)
                return NoContent();
            var result = _mapper.Map<IEnumerable<UserTask>, IEnumerable<UserTaskResultDto>>(tasks);
            return Ok(result);
        }
    }
}