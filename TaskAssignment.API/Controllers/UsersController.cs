﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TaskAssignment.API.Models;
using TaskAssignment.DAL.Root;

namespace TaskAssignment.API.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericUnitOfWork _uow;
        public UsersController(IMapper mapper, IGenericUnitOfWork uow)
        {
            _mapper = mapper;
            _uow = uow;
        }

        [HttpGet]
        [SwaggerOperation(Tags = new[] { "Users" })]
        [ProducesResponseType(typeof(IEnumerable<UserResultDto>), 200)]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _uow.GetRepository<DAL.Entities.User>().GetOverview();
            if (users == null)
                return NoContent();
            var result = _mapper.Map<IEnumerable<DAL.Entities.User>, IEnumerable<UserResultDto>>(users);
            return Ok(result);
        }
    }
}