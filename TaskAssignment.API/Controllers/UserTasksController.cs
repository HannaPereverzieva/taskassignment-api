﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TaskAssignment.API.Models;
using TaskAssignment.DAL.Entities;
using TaskAssignment.DAL.Root;

namespace TaskAssignment.API.Controllers
{
    [Produces("application/json")]
    [Route("api")]
    public class UserTasksController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGenericUnitOfWork _uow;
        public UserTasksController(IMapper mapper, IGenericUnitOfWork uow)
        {
            _mapper = mapper;
            _uow = uow;
        }

        [HttpPost]
        [Route("tasks")]
        [SwaggerOperation(Tags = new[] { "User tasks" })]
        [ProducesResponseType(typeof(UserTaskResultDto), 201)]
        public async Task<IActionResult> AddTask([FromBody]UserTaskRequestDto taskDto)
        {
            var repository = _uow.GetRepository<TaskGroup>();
            var group = await repository.GetDetails(x => x.Id == taskDto.TaskGroupId);
            if (group != null)
            {
                var task = _mapper.Map<UserTaskRequestDto, UserTask>(taskDto);
                await _uow.GetRepository<UserTask>().Add(task);
                await _uow.SaveAsync();
                var result = _mapper.Map<UserTask, UserTaskResultDto>(task);
                return CreatedAtAction("GetTask", new { taskId = result.Id }, result);
            }
            return NotFound();
        }

        [HttpPatch]
        [Route("tasks/{taskId}")]
        [SwaggerOperation(Tags = new[] { "User tasks" })]
        [ProducesResponseType(204)]
        public async Task<IActionResult> UpdateTask([FromRoute]int taskId, [FromBody]UserTaskRequestDto taskDto)
        {
            var repository = _uow.GetRepository<UserTask>();
            var existTask = await repository.GetDetails(x => x.Id == taskId);
            if (existTask != null)
            {
                _mapper.Map(taskDto, existTask);
                repository.Update(existTask);
                await _uow.SaveAsync();
                return Ok();
            }
            return NotFound();
        }

        [HttpGet]
        [Route("tasks/{taskId}")]
        [SwaggerOperation(Tags = new[] { "User tasks" })]
        [ProducesResponseType(typeof(UserTaskResultDto), 200)]
        public async Task<IActionResult> GetTask([FromRoute] int taskId)
        {
            var task = await _uow.GetRepository<UserTask>().GetDetails(x => x.Id == taskId);
            if (task == null)
                return NoContent();
            var result = _mapper.Map<UserTask, UserTaskResultDto>(task);
            return Ok(result);
        }

        [HttpDelete]
        [Route("tasks/{taskId}")]
        [SwaggerOperation(Tags = new[] { "User tasks" })]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteTask([FromRoute] int taskId)
        {
            var repository = _uow.GetRepository<UserTask>();
            var entity = await repository.GetDetails(x => x.Id == taskId);
            if (entity != null)
            {
                repository.Delete(entity);
                await _uow.SaveAsync();
                return Ok();
            }

            return NotFound();
        }

        [HttpPut]
        [Route("tasks/assign")]
        [SwaggerOperation(Tags = new[] { "User tasks" })]
        [ProducesResponseType(200)]
        public async Task<IActionResult> AssignTask([FromQuery]int taskId, [FromQuery]int userId)
        {
            var existTask = await _uow.GetRepository<UserTask>().GetDetails(x => x.Id == taskId);
            var existUser = await _uow.GetRepository<User>().GetDetails(x => x.Id == userId);

            if (existTask != null && existUser != null)
            {
                existTask.UserId = userId;
                await _uow.SaveAsync();
                return Ok();
            }
            return NotFound();
        }
    }
}